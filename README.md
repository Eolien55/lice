`lice` is basically choosealicense.com but offline.

Put `lice` in `$XDG_DATA_HOME`, usually `$HOME/.local/share`.

There is :
- `lice.sh` which uses slap (YAML -> rust clap parser -> shell variables;
  very ew when I think about it)
- `lice.rc` which does the same but in base Plan 9's `rc` shell, with the
  plan 9 utils
- `lice` which is implemented in rust and is quite literally just a
  frontend to GNU readline (🤮) `envsubst`
