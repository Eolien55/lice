#!/bin/bash
slap deps envsubst || exit 1
eval "$(slap parse bash _ -- "$@" <<EOF
name: lice
version: "1.0"
about: |
  Offline version of choosealicense.com
  
  Template files are located in $XDG_DATA_HOME/lice ($HOME/.local/share/lice by
  default) and are separated in 3 categories :
  
  plain-text license file (code : license)
  
  header of said license where applicable (a boilerplate indicating the
  license) (code : header)
  
  a description of said license, taken from GitHub'\''s choosealicense.com. Thanks
  to them for providing the data.

settings:
  - ArgRequiredElseHelp
  - ColorAlways

global_settings:
  - ColoredHelp

args:
  - license:
      help: specify license to print
      required: true
      default_value: default
  - year:
      help: specify year in license
      long: year
      short: y
      default_value: "$(date +%Y)"
  - username:
      help: specify organization's name in license
      long: username
      short: n
      default_value: "$(grep "^$(id -nu)" /etc/passwd | cut -d: -f5)"
  - project:
      help: specify project name in license
      long: project
      short: p
      default_value: "$(basename "$(git rev-parse --show-toplevel 2>/dev/null || echo "$PWD")")"
  - homepage:
      help: specify homepage name in license
      long: homepage
      short: H
      takes_value: true
  - mode:
      help: specify mode, ie cateogry of file to be printed
      long: mode
      short: m
      possible_values: [license, header, description]
      default_value: license
  - list:
      help: list files in category
      long: list
      short: l
EOF
)"; [[ -z "${_success}" ]] && exit 1

for i in year username project homepage mode license; do
	val=_${i}_vals
	eval "export $i=\${$val}"
done

envsubst <lice/$mode/$license
