Creative Commons Attribution Share Alike 4.0 International

Similar to CC-BY-4.0 but requires derivatives be distributed under the same or a similar, [compatible](https://creativecommons.org/compatiblelicenses/) license. Frequently used for media assets and educational materials. A previous version is the default license for Wikipedia and other Wikimedia projects. Not recommended for software.

permissions:
    The licensed material and derivatives may be used for commercial purposes.
    The licensed material may be modified.
    The licensed material may be distributed.
    The licensed material may be used and modified in private.

conditions:
    A copy of the license and copyright notice must be included with the licensed material.
    Changes made to the licensed material must be documented.
    Modifications must be released under the same license when distributing the licensed material. In some cases a similar or related license may be used.

limitations:
    This license includes a limitation of liability.
    This license explicitly states that it does NOT grant trademark rights, even though licenses without such a statement probably do not grant any implicit trademark rights.
    This license explicitly states that it does NOT grant any rights in the patents of contributors.
    This license explicitly states that it does NOT provide any warranty.
