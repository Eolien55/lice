use chrono::Datelike;
use clap::Parser;
use dirs::data_dir;

use std::env;
use std::fs;
use std::path::PathBuf;
use std::process::Command;

const POSSIBLE_MODES: &[&str] = &["license", "header", "description"];

#[derive(Debug, Clone, Parser)]
#[clap(author)]
/// Offline version of choosealicense.com
///
/// Template files are located in $XDG_DATA_HOME/lice ($HOME/.local/share/lice by default) and are
/// separated in 3 categories :
///
/// plain-text license file (code : license)
///
/// header of said license where applicable (a boilerplate indicating the license) (code : header)
///
/// a description of said license, taken from GitHub's choosealicense.com. Thanks to them for providing
/// the data.
struct Cli {
    /// Specify license
    #[clap(default_value = "default")]
    license: String,

    /// Specify year
    #[clap(long, short)]
    year: Option<String>,

    /// Specify organization
    #[clap(long, short)]
    username: Option<String>,

    /// Specify project's name
    #[clap(long, short)]
    project: Option<String>,

    /// Specify homepage's URL
    #[clap(long, short = 'H')]
    homepage: Option<String>,

    /// Specify project's description
    #[clap(long, short)]
    description: Option<String>,

    /// Specify mode, ie category of file used
    #[clap(long, short, possible_values = POSSIBLE_MODES, default_value = "license")]
    mode: String,

    /// List files in category
    #[clap(long, short)]
    list: bool,
}

macro_rules! itm {
    ($a:expr) => {
        (quote::quote!($a).to_string(), $a)
    };
}

macro_rules! apply {
    ($macro:ident, $($a:expr),+) => {
        [$($macro!($a)),+]
    }
}

fn main() -> std::io::Result<()> {
    let cli = Cli::parse();

    let license = cli.license;
    let mut path = PathBuf::new();

    path.push(&data_dir().expect("Cannot construct path to data"));
    path.push("lice");
    path.push(&cli.mode);
    if cli.list {
        let paths = fs::read_dir(&path).unwrap();
        for path in paths {
            println!(
                "{}",
                path.expect("listing files")
                    .path()
                    .file_name()
                    .expect("getting file's basename")
                    .to_str()
                    .expect("utf-8 invalid file while listing")
            );
        }

        return Ok(());
    }

    path.push(&license);

    let year = cli.year.unwrap_or_else(|| chrono::Local::now().year().to_string());
    let username = cli.name.unwrap_or_else(whoami::realname);
    let project = cli.project.unwrap_or_else(||
        env::current_dir()
            .expect("Cannot get current directory")
            .file_name()
            .expect("Cannot get current directory")
            .to_str()
            .expect("Your path isn't valid UTF-8")
            .into(),
    );
    let homepage = cli
        .homepage
        .unwrap_or_else(|| "SET THE PROJECT'S HOMEPAGE VIA THE HOMEPAGE FLAG".to_string());
    let desc = cli
        .description
        .unwrap_or_else(|| "SET THE PROJECT'S DESCRIPTION VIA THE DESCRIPTION FLAG".to_string());

    for (varname, _) in env::vars() {
        env::remove_var(varname);
    }

    for (name, value) in apply!(itm, year, username, project, homepage, desc) {
        env::set_var(name, value);
    }

    if let Ok(file) = fs::File::open(&path) {
        Command::new("envsubst")
            .stdin(file)
            .spawn()
            .expect("envsubst failed to start")
            .wait()
            .ok();
    } else {
        eprintln!("No {}: {}", cli.mode, license);
        std::process::exit(1);
    }

    Ok(())
}
